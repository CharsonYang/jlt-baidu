# jlt-baidu

#### 介绍
包含了如何使用百度AI的文本相似度，OCR图文识别，表格识别的相关用法；包含2019年国家最新地址库的json；包含pdf转成图片功能；使用tesseract4+opencv3.4.1支持任意图片全文识别，excel表格识别，任意图片裁剪出excel区域；

#### 软件架构
软件架构说明


#### 安装教程

1.  安装JDK1.8
2.  安装Maven3+
3.  无需安装tesseract，opencv
4.  使用百度AI功能需要申请百度相关的应用Appid,此处涉及图文识别应用

#### 使用说明

1.  配置Maven，下载相应的包
2.  配置JVM运行参数：-Djava.library.path=F:\workSpace\JLTMdmWorkSpace\jlt-baidu\src\main\resources\opencv\java\x64(替换本地工程路径)
3.  修改application.yml里面的配置信息，指定相应路径
4.  默认端口9174，已经支持swaggerUi，访问路径：http://localhost:9174/doc.html
5.  访问路径：http://localhost:9174/

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
