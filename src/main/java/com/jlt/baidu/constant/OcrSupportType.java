package com.jlt.baidu.constant;

/**
 * 能ocr识别的类型，支持图片，pdf
 * 
 * @author 苹果
 * @date 2020/01/06
 */
public class OcrSupportType {
    public static final String IMAGE_PNG = "png";
    public static final String IMAGE_JPG = "jpg";
    public static final String IMAGE_JPEG = "jpeg";
    public static final String IMAGE_BMP = "bmp";

    public static final String FILE_PDF = "pdf";
}
