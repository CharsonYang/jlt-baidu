package com.jlt.baidu.constant;

/**
 * 调用百度OCR的错误信息
 * 
 * @author 苹果
 * @date 2020/01/03
 */
public enum BaiduOcrErrorEnum {
    ERROR_10000("10000", "图片主体请保持竖直，不要倾斜或横向"), ERROR_0("0", "请求成功"), ERROR_4("4", "请求集群超限额"),
    ERROR_6("6", "无权限访问该用户数据，应用ID不在"), ERROR_17("17", "请求每天请求量超限额"), ERROR_18("18", "请求QPS超限额"),
    ERROR_19("19", "请求总量超限额"), ERROR_216100("216100", "请求中包含非法参数"), ERROR_216102("216102", "请求了不支持的服务，请检查调用的url"),
    ERROR_216110("216110", "appid不存在,请核对应用"), ERROR_216200("216200", "图片为空"),
    ERROR_216201("216201", "仅支持图片格式为：PNG、JPG、JPEG、BMP"), ERROR_216202("216202", "图片大小超过4M或太小1k"),
    ERROR_216630("216630", "图片识别错误，请在控制台提交工单联系技术支持团队"), ERROR_272000("272000", "图片未能匹配到模板"),
    ERROR_272001("272001", "图片未能匹配到分类"), ERROR_272002("272002", "图片成功匹配到分类，未能匹配到分类中的模板"),
    ERROR_282000("282000", "百度服务内部错误，识别失败"), ERROR_282103("282103", "图片目标识别错误，请在控制台提交工单联系技术支持团队"),
    ERROR_110("110", "请求百度token失效"), ERROR_111("111", "请求百度token过期"), ERROR_10086("10086", "暂仅支持图片格式与pdf文件识别");

    String code;
    String name;

    BaiduOcrErrorEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getErrorMessage(String code) {
        for (BaiduOcrErrorEnum tEnum : values()) {
            if (tEnum.getCode() == code) {
                return tEnum.getName();
            }
        }
        return "未知错误";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
