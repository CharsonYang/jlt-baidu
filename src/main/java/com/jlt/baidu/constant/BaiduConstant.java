package com.jlt.baidu.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class BaiduConstant {
//百度账号的api+key
    public static final String OCR_API_KEY = "";
    public static final String OCR_SECRET_KEY = "";
    public static final String OCR_QRCODE_URL = "https://aip.baidubce.com/rest/2.0/ocr/v1/qrcode?access_token=";
    public static final String OCR_EXCEL_URL = "https://aip.baidubce.com/rest/2.0/ocr/v1/form?access_token=";
    public static final String OCR_EXCEL_SYNC_REQ_URL =
        "https://aip.baidubce.com/rest/2.0/solution/v1/form_ocr/request?access_token=";
    public static final String OCR_EXCEL_SYNC_RES_URL =
        "https://aip.baidubce.com/rest/2.0/solution/v1/form_ocr/get_request_result?access_token=";
    public static final String OCR_TEMPLAT_URL =
        "https://aip.baidubce.com/rest/2.0/solution/v1/iocr/recognise?access_token=";
    // 线下运营 回单业务分类器Id
    public static final int OCR_CLASS_RECEIPT = 1;
    // 预约单业务
    public static final int OCR_CLASS_BOOK = 2;
}
