package com.jlt.baidu.utils;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import lombok.experimental.UtilityClass;

/**
 * 图片高级处理，腐蚀，膨胀
 * 
 * @author Ives.Chen
 *
 */
@UtilityClass
public class OpencvDialteUtil {
    /*
     * 高级图像处理
     * 
     * 开运算MORPH_OPEN：先腐蚀再膨胀，用来消除小物体
     * 
     * 闭运算MORPH_CLOSE：先膨胀再腐蚀，用于排除小型黑洞
     * 
     * 形态学梯度MORPH_GRADIENT：就是膨胀图与俯视图之差，用于保留物体的边缘轮廓。
     * 
     * 顶帽MORPH_TOPHAT：原图像与开运算图之差，用于分离比邻近点亮一些的斑块。
     * 
     * 黑帽MORPH_BLACKHAT：闭运算与原图像之差，用于分离比邻近点暗一些的斑块。
     * 
     * 腐蚀 MORPH_ERODE 膨胀 MORPH_DILATE
     */
    /**
     * 对图片进行腐蚀处理，采用普通处理
     * 
     * @param source
     */
    public static Mat commonDialate(Mat source) {
        // 过时方法
        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(20, 20));
        Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(10, 10));
        Mat dilate1Element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(20, 20));
        Mat erode1Element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 25));
        // 膨胀
        Mat dilate = source.clone();
        Imgproc.dilate(source, dilate, dilateElement);
        // 腐蚀
        Mat erode = dilate.clone();
        Imgproc.erode(dilate, erode, erodeElement);
        // 膨胀
        Mat dilate1 = erode.clone();
        Imgproc.dilate(erode, dilate1, dilate1Element);
        // 腐蚀
        Mat erode1 = dilate1.clone();
        Imgproc.erode(dilate1, erode, erode1Element);
        return erode1;
    }

    /**
     * 对图片进行腐蚀处理，采用顶帽算法，清晰筛选出表格的边框,测试良好
     * 
     * 使用场景：表格识别
     * 
     * @param source
     */
    public static Mat excelDialate(Mat source) {
        // 过时方法
        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(50, 150));
        Mat erode = source.clone();
        Imgproc.morphologyEx(source, erode, Imgproc.MORPH_TOPHAT, dilateElement);
        return erode;
    }
}
