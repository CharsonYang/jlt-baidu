package com.jlt.baidu.utils;


import lombok.experimental.UtilityClass;

/**
 * 图片过程保存工具类
 * 
 * @author Ives.Chen
 *
 */
@UtilityClass
public class ImageProcessSaveUtil {
    /**
     * 保存去除印章之后的图片
     * 
     * @param path
     * @param fileType
     * @return
     */
    public static String processRedUrl(String path, String uuid, String fileType) {
        return concatString(path, uuid, "red", fileType);
    }

    /**
     * 保存标记划线轮廓的图片
     * 
     * @param path
     * @param fileType
     * @return
     */
    public static String processLineUrl(String path, String uuid, String fileType) {
        return concatString(path, uuid, "line", fileType);
    }

    /**
     * 保存标记划线轮廓的图片
     * 
     * @param path
     * @param fileType
     * @return
     */
    public static String processDialetUrl(String path, String uuid, String fileType) {
        return concatString(path, uuid, "dialet", fileType);
    }

    private static String concatString(String path, String uuid, String type, String fileType) {
        return new StringBuilder(path).append(uuid).append("_").append(type).append(".").append(fileType).toString();
    }
}
