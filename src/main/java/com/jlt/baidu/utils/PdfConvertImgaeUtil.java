package com.jlt.baidu.utils;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

/**
 * PDF转成图片
 * 
 * @author 苹果
 * @date 2020/01/08
 */
public class PdfConvertImgaeUtil {
    /**
     * pdf转成图片
     * 
     * @param filePath
     * @return
     */
    public static List<BufferedImage> pdf2png(String filePath) {
        // 将pdf装图片 并且自定义图片得格式大小
        File file = new File(filePath);
        try {
            PDDocument doc = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(doc);
            int pageCount = doc.getNumberOfPages();
            List<BufferedImage> images = new ArrayList<>();
            for (int i = 0; i < pageCount; i++) {
                BufferedImage image = renderer.renderImageWithDPI(i, 144); // Windows native DPI
                images.add(image);
            }
            return images;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 转换全部的pdf
     * 
     * @param fileAddress
     *            文件地址
     * @param filename
     *            PDF文件名
     * @param type
     *            图片类型
     */
    public static List<BufferedImage> pdf2png(String fileAddress, String filename, String type) {
        // 将pdf装图片 并且自定义图片得格式大小
        File file = new File(fileAddress + "\\" + filename + ".pdf");
        try {
            PDDocument doc = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(doc);
            int pageCount = doc.getNumberOfPages();
            List<BufferedImage> images = new ArrayList<>();
            for (int i = 0; i < pageCount; i++) {
                BufferedImage image = renderer.renderImageWithDPI(i, 144); // Windows native DPI
                images.add(image);
            }
            return images;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 自由确定起始页和终止页
     * 
     * @param fileAddress
     *            文件地址
     * @param filename
     *            pdf文件名
     * @param indexOfStart
     *            开始页 开始转换的页码，从0开始
     * @param indexOfEnd
     *            结束页 停止转换的页码，-1为全部
     * @param type
     *            图片类型
     */
    public static void pdf2png(String fileAddress, String filename, int indexOfStart, int indexOfEnd, String type) {
        // 将pdf装图片 并且自定义图片得格式大小
        File file = new File(fileAddress + "\\" + filename + ".pdf");
        try {
            PDDocument doc = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(doc);
            int pageCount = doc.getNumberOfPages();
            for (int i = indexOfStart; i < indexOfEnd; i++) {
                BufferedImage image = renderer.renderImageWithDPI(i, 144); // Windows native DPI
                ImageIO.write(image, type, new File(fileAddress + "\\" + filename + "_" + (i + 1) + "." + type));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 多张图片合并成一张，要求图片宽度一样
     */
    public static BufferedImage imagerManyMerageOne(List<BufferedImage> images) {
        if (images == null || images.size() == 0) {
            return null;
        }
        // 获取图片的宽度
        int width = images.get(0).getWidth();
        int totalHeight = 0;
        for (BufferedImage image : images) {
            totalHeight += image.getHeight();
        }
        // 构造一个类型为预定义图像类型之一的 BufferedImage。 宽度为第一只的宽度，高度为各个图片高度之和
        BufferedImage taget = new BufferedImage(width, totalHeight, BufferedImage.TYPE_INT_RGB);
        // 绘制合成图像
        Graphics g = taget.createGraphics();
        int processHeight = 0;
        for (int i = 0; i < images.size(); i++) {
            BufferedImage image = images.get(i);
            if (i == 0) {
                g.drawImage(image, 0, processHeight, width, image.getHeight(), null);
                processHeight += image.getHeight();
            } else {
                g.drawImage(image, 0, processHeight, width, image.getHeight(), null);
                processHeight += image.getHeight();
            }
        }
        // 释放此图形的上下文以及它使用的所有系统资源。
        g.dispose();
        return taget;
    }

    public static void main(String[] args) throws IOException {
        List<BufferedImage> sas = pdf2png("D:\\workSpace\\ocr\\pdf", "2019-12-12送天津4", "png");
        // 创建输出流
        int i = 0;
        for (BufferedImage result : sas) {
            ImageIO.write(result, "png", new File("D:\\workSpace\\ocr\\pdf" + "\\2019-12-12送天津4号_" + (i + 1) + ".png"));
            i++;
        }
    }
}
