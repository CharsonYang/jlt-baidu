package com.jlt.baidu.utils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 整合后的全拼地址
 * 
 * @author 苹果
 * @date 2019/10/18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
public class LocaltionJsonModel {
    String addressCn;
    String addressEn;
    String addressSt;
}
