package com.jlt.baidu.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jlt.baidu.utils.Opencv420Util;

@RestController
@RequestMapping("/opencv")
public class OpencvController {
    @PostMapping("/zoom")
    public void testImageZoom() {
        Opencv420Util.imageZoom(Opencv420Util.imagerLoad("D:\\workSpace\\ocr\\b036c01d28880a39ebd9e76f568b36d.jpg"));
    }
}
