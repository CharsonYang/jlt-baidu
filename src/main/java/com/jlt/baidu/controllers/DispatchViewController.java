package com.jlt.baidu.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 页面中转
 * 
 * @author Ives.Chen
 *
 */
@Controller
public class DispatchViewController {
    @RequestMapping("/baidu")
    public String dispatchBaidu() {
        return "/baidu-index.html";
    }

    @RequestMapping("/tesseract")
    public String dispatchBaidu(@RequestParam String type) {
        if ("1".equals(type)) {
            return "/ocr-excel-index.html";
        }
        if ("2".equals(type)) {
            return "/not-excel-index.html";
        }
        return "/ocr-index.html";
    }
}
