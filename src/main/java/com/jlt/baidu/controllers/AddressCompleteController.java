package com.jlt.baidu.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jlt.baidu.algorithm.ConsineHandle;
import com.jlt.baidu.algorithm.PentahoJaroWinklerDistance;
import com.jlt.baidu.dto.AddressRequest;
import com.jlt.baidu.dto.DepparserResponse;
import com.jlt.baidu.dto.SimilaryRequest;
import com.jlt.baidu.utils.BaidduAIUtil;
import com.jlt.baidu.utils.LocaltionJsonModel;
import com.jlt.baidu.utils.StandardLocaltionUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 调度百度API地址识别 https://ai.baidu.com/docs#/NLP-Apply-API/582ca266
 * 
 * 对于百度AI地址补全的要求：省，区可以忽略，市一定要必填
 * 
 * 对于4级地址补全的要求：省可以忽略，市，区一定要写
 * 
 * @author 苹果
 * @date 2019/10/22
 */
@Api
@Slf4j
@RestController
@RequestMapping("/address")
public class AddressCompleteController {
    @Autowired
    StandardLocaltionUtil localtionUtil;

    /**
     * 按照kettle算法计算文本相似度
     * 
     * @return
     */
    @ApiOperation(value = "", notes = "按照kettle算法计算文本相似度")
    @PostMapping("/testLKettleSimilary")
    @ResponseBody
    public Map<String, String> testLKettleSimilary(@RequestParam String text1, @RequestParam String text2) {
        Map<String, String> similaryMap = new HashMap<>();
        PentahoJaroWinklerDistance distance = new PentahoJaroWinklerDistance();
        double resTmp = distance.apply(text1, text2);
        similaryMap.put(text1 + "与" + text2 + "相似度(kettle算法)：", String.format("%.2f", resTmp));
        return similaryMap;
    }

    /**
     * 按照余弦相似度算法计算文本相似度
     * 
     * @return
     */
    @ApiOperation(value = "", notes = "按照余弦相似度算法计算文本相似度")
    @PostMapping("/testConsineSimilary")
    @ResponseBody
    public Map<String, String> testConsineSimilary(@RequestParam String text1, @RequestParam String text2) {
        Map<String, String> similaryMap = new HashMap<>();
        double resTmp = ConsineHandle.consieSimilary(text1, text2);
        similaryMap.put(text1 + "与" + text2 + "相似度(余弦相似度算法)：", String.format("%.2f", resTmp));
        return similaryMap;
    }

    /**
     * 从3级地址库测试kettle算法
     * 
     * @param address
     * @return
     */
    @ApiOperation(notes = "测试3级地址库下，kettle相似度算法", value = "")
    @PostMapping("/Testkettle3Level")
    public Map<String, String> Testkettle3Level(@RequestBody String address) {
        List<LocaltionJsonModel> result = localtionUtil.readJsonFile("standardLocation.json");
        // 相似度结果
        Map<String, String> similaryMap = new HashMap<>();
        PentahoJaroWinklerDistance distance = new PentahoJaroWinklerDistance();
        distance.reset();
        double maxTmp = 0D;
        String addressTmp = null;
        for (LocaltionJsonModel localtionJsonModel : result) {
            // double resTmp = ConsineHandle.consieSimilary(address, localtionJsonModel.getAddressCn());
            String target = localtionJsonModel.getAddressCn().substring(2, localtionJsonModel.getAddressCn().length());
            double resTmp = distance.apply(address, target);
            if (resTmp >= maxTmp) {
                maxTmp = resTmp;
                addressTmp = target;
            }
        }

        similaryMap.put(address + "与" + addressTmp + "相似度(三级地址库)：", String.format("%.2f", maxTmp));
        return similaryMap;
    }

    /**
     * 从4级地址库测试kettle算法
     * 
     * @param address
     * @return
     */
    @ApiOperation(notes = "测试4级地址库下，kettle相似度算法", value = "")
    @PostMapping("/Testkettle4Level")
    public Map<String, String> Testkettle4Level(@RequestBody String address) {
        List<LocaltionJsonModel> result = localtionUtil.readJsonFile("standardFourLevelLocation.json");
        // 相似度结果
        Map<String, String> similaryMap = new HashMap<>();
        PentahoJaroWinklerDistance distance = new PentahoJaroWinklerDistance();
        distance.reset();
        double maxTmp = 0D;
        String addressTmp = null;
        for (LocaltionJsonModel localtionJsonModel : result) {
            // double resTmp = ConsineHandle.consieSimilary(address, localtionJsonModel.getAddressCn());
            double resTmp = distance.apply(address, localtionJsonModel.getAddressCn());
            if (resTmp >= maxTmp) {
                maxTmp = resTmp;
                addressTmp = localtionJsonModel.getAddressCn();
            }

            double resTmp2 = ConsineHandle.consieSimilary(address, localtionJsonModel.getAddressSt());
            if (resTmp2 >= maxTmp) {
                maxTmp = resTmp2;
                addressTmp = localtionJsonModel.getAddressSt();
            }
        }
        similaryMap.put(address + "与" + addressTmp + "相似度(4级地址库)：", String.format("%.2f", maxTmp));
        return similaryMap;
    }

    /**
     * 从5级地址库测试kettle算法
     * 
     * @param address
     * @return
     */
    @ApiOperation(notes = "测试5级地址库下，kettle相似度算法", value = "")
    @PostMapping("/Testkettle5Level")
    public Map<String, String> Testkettle5Level(@RequestBody String address) {
        List<LocaltionJsonModel> result = localtionUtil.readJsonFile("standardFiveLevelLocation.json");
        // 相似度结果
        Map<String, String> similaryMap = new HashMap<>();
        PentahoJaroWinklerDistance distance = new PentahoJaroWinklerDistance();
        distance.reset();
        double maxTmp = 0D;
        String addressTmp = null;
        for (LocaltionJsonModel localtionJsonModel : result) {
            // double resTmp = ConsineHandle.consieSimilary(address, localtionJsonModel.getAddressCn());
            double resTmp = distance.apply(address, localtionJsonModel.getAddressCn());
            if (resTmp >= maxTmp) {
                maxTmp = resTmp;
                addressTmp = localtionJsonModel.getAddressCn();
            }
        }
        similaryMap.put(address + "与" + addressTmp + "相似度(5级地址库)：", String.format("%.2f", maxTmp));
        return similaryMap;
    }

    /**
     * 从3级地址库测试kettle算法
     * 
     * @param address
     * @return
     */
    @ApiOperation(notes = "测试3级地址库下，余弦相似度算法", value = "")
    @PostMapping("/TestConsine3Level")
    public Map<String, String> TestConsine3Level(@RequestBody String address) {
        List<LocaltionJsonModel> result = localtionUtil.readJsonFile("standardLocation.json");
        // 相似度结果
        Map<String, String> similaryMap = new HashMap<>();
        PentahoJaroWinklerDistance distance = new PentahoJaroWinklerDistance();
        distance.reset();
        double maxTmp = 0D;
        String addressTmp = null;
        for (LocaltionJsonModel localtionJsonModel : result) {
            String target = localtionJsonModel.getAddressCn().substring(2, localtionJsonModel.getAddressCn().length());
            double resTmp = ConsineHandle.consieSimilary(address, target);
            if (resTmp >= maxTmp) {
                maxTmp = resTmp;
                addressTmp = target;
            }
        }

        similaryMap.put(address + "与" + addressTmp + "相似度(三级地址库)：", String.format("%.2f", maxTmp));
        return similaryMap;
    }

    /**
     * 从4级地址库测试kettle算法
     * 
     * @param address
     * @return
     */
    @ApiOperation(notes = "测试4级地址库下，余弦相似度算法", value = "")
    @PostMapping("/TestConsine4Level")
    public Map<String, String> TestConsine4Level(@RequestBody String address) {
        List<LocaltionJsonModel> result = localtionUtil.readJsonFile("standardFourLevelLocation.json");
        // 相似度结果
        Map<String, String> similaryMap = new HashMap<>();
        PentahoJaroWinklerDistance distance = new PentahoJaroWinklerDistance();
        distance.reset();
        double maxTmp = 0D;
        String addressTmp = null;
        for (LocaltionJsonModel localtionJsonModel : result) {
            double resTmp = ConsineHandle.consieSimilary(address, localtionJsonModel.getAddressCn());
            if (resTmp >= maxTmp) {
                maxTmp = resTmp;
                addressTmp = localtionJsonModel.getAddressCn();
            }

            double resTmp2 = ConsineHandle.consieSimilary(address, localtionJsonModel.getAddressSt());
            if (resTmp2 >= maxTmp) {
                maxTmp = resTmp2;
                addressTmp = localtionJsonModel.getAddressSt();
            }
        }
        similaryMap.put(address + "与" + addressTmp + "相似度(4级地址库)：", String.format("%.2f", maxTmp));
        return similaryMap;
    }

    /**
     * 从5级地址库测试kettle算法
     * 
     * @param address
     * @return
     */
    @ApiOperation(notes = "测试5级地址库下，余弦相似度算法", value = "")
    @PostMapping("/TestConsine5Level")
    public Map<String, String> TestConsine5Level(@RequestBody String address) {
        List<LocaltionJsonModel> result = localtionUtil.readJsonFile("standardFiveLevelLocation.json");
        // 相似度结果
        Map<String, String> similaryMap = new HashMap<>();
        PentahoJaroWinklerDistance distance = new PentahoJaroWinklerDistance();
        distance.reset();
        double maxTmp = 0D;
        String addressTmp = null;
        for (LocaltionJsonModel localtionJsonModel : result) {
            double resTmp = ConsineHandle.consieSimilary(address, localtionJsonModel.getAddressCn());
            if (resTmp >= maxTmp) {
                maxTmp = resTmp;
                addressTmp = localtionJsonModel.getAddressCn();
            }
        }
        similaryMap.put(address + "与" + addressTmp + "相似度(5级地址库)：", String.format("%.2f", maxTmp));
        return similaryMap;
    }

    /**
     * 测试百度AI的地址补全
     * 
     * @param condition
     * @return
     */
    @ApiOperation(notes = "测试百度AI地址自动补全", value = "")
    @PostMapping("/TestBaiduAi")
    public String TestBaiduAiAddress(@RequestBody AddressRequest condition) {
        return BaidduAIUtil.AddressComplete(condition);
    }

    /**
     * 测试百度的文本相似度
     * 
     * @param condition
     * @return
     */
    @ApiOperation(notes = "测试百度短文本相似度功能", value = "")
    @PostMapping("/testBaiduSimilary")
    public String testBaiduSimilary(@RequestBody SimilaryRequest condition) {
        return BaidduAIUtil.textSimilaryPrecent(condition);
    }

    /**
     * 测试百度词法分析功能
     * 
     * @param text
     * @return
     */
    @ApiOperation(notes = "测试百度词法分析功能", value = "")
    @PostMapping("/testLexerFunction")
    public String testLexerFunction(@RequestBody String text) {
        return BaidduAIUtil.testLexerFunction(text);
    }

    /**
     * 测试百度词法分析功能
     * 
     * @param text
     * @return
     */
    @ApiOperation(notes = "测试百度AI的依存词法分析接口", value = "")
    @PostMapping("/testDepparserFunction")
    @ResponseBody
    public List<DepparserResponse> testDepparserFunction(@RequestBody String text) {
        return BaidduAIUtil.testDepparserFunction(text);
    }

}
