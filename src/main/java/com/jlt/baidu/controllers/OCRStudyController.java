package com.jlt.baidu.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jlt.baidu.dto.CustomExcelResponse;
import com.jlt.baidu.dto.CustomOcrResponse;
import com.jlt.baidu.dto.FileOcrModel;
import com.jlt.baidu.dto.QrcodeResponse;
import com.jlt.baidu.service.BaiduOcrService;
import com.jlt.baidu.utils.BaiduOCRUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * OCR识别控制
 * 
 * @author 苹果
 * @date 2019/12/21
 */
@Api
@Slf4j
@RestController
@RequestMapping("/ocr")
public class OCRStudyController {
    @Autowired
    BaiduOcrService ocrService;

    @ApiOperation(value = "", notes = "天猫模板分类器--计划采购单--业务处理")
    @PostMapping("/testTianmaoReceipt")
    @ResponseBody
    public FileOcrModel testTianmaoReceipt() {
        String image1 = "D:\\workSpace\\ocr\\tianma\\daohuo\\2132_20191226101701.png";
        FileOcrModel model = new FileOcrModel();
        model.setFileSaveUrl(image1);
        model.setFileType("png");
        return ocrService.disReceiptForUnderline(model);
    }

    @ApiOperation(value = "", notes = "天猫模板分类器--预约单--业务处理")
    @PostMapping("/testTianmaoBook")
    @ResponseBody
    public FileOcrModel testTianmaoBook() {
        String file = "D:\\workSpace\\ocr\\pdf\\2019-12-12送天津4.pdf";
        FileOcrModel model = new FileOcrModel();
        model.setFileSaveUrl(file);
        model.setFileType("pdf");
        return ocrService.disBookForUnderline(model);
    }

    @ApiOperation(value = "", notes = "天猫模板识别--到货通知单")
    @PostMapping("/testTianmaoDaohuo")
    @ResponseBody
    public CustomOcrResponse testTianmaoDaohuo() {
        String image1 = "D:\\workSpace\\ocr\\tianma\\daohuo\\26ad1f5c419979b66a1b56700bc4f31_narow.jpg";
        return BaiduOCRUtil.ocrImageTemplate(BaiduOCRUtil.GetImageBase64(image1), "fc7ff9d6382c06b019a98c0a42d853af");
    }

    @ApiOperation(value = "", notes = "天猫模板识别--计划采购单")
    @PostMapping("/testTianmaoCaigou")
    @ResponseBody
    public CustomOcrResponse testTianmaoCaigou() {
        String image1 = "D:\\workSpace\\ocr\\tianma\\caigou\\微信图片_20191226101824.jpg";
        return BaiduOCRUtil.ocrImageTemplate(BaiduOCRUtil.GetImageBase64(image1), "18a365914fbdb83f2a7fbaf4c6157220");
    }

    @ApiOperation(value = "", notes = "天猫模板分类器--计划采购单")
    @PostMapping("/testTianmaoClassFiler")
    @ResponseBody
    public CustomOcrResponse testTianmaoClassFiler() {
        String image1 = "D:\\workSpace\\ocr\\tianma\\caigou\\20191226101853_narow.jpg";
        return BaiduOCRUtil.ocrImageClassifier(BaiduOCRUtil.GetImageBase64(image1), 1);
    }

    @ApiOperation(value = "", notes = "天猫预约单--分类器模板--预约单头")
    @PostMapping("/testYuHead")
    @ResponseBody
    public CustomOcrResponse testYuHead() {
        String image1 = "D:\\workSpace\\ocr\\pdf\\2019-12-12送天津2号_1.png";
        return BaiduOCRUtil.ocrImageTemplate(BaiduOCRUtil.GetImageBase64(image1), "a1f9c4066d406ac6bc5b1b3d0f8f4e8a");
    }

    @ApiOperation(value = "", notes = "天猫预约单--分类器模板--预约单内容")
    @PostMapping("/testYuBody")
    @ResponseBody
    public CustomOcrResponse testYuBody() {
        String image1 = "D:\\workSpace\\ocr\\pdf\\2019-12-12送天津2号_2.png";
        return BaiduOCRUtil.ocrImageTemplate(BaiduOCRUtil.GetImageBase64(image1), "eed7c4c62c31d0b222fcac4ffa4f9ef9");
    }

    @ApiOperation(value = "", notes = "天猫预约单--表格同步识别--结果")
    @PostMapping("/testExcelRes")
    @ResponseBody
    public CustomExcelResponse testExcelRes() {
        String image1 = "D:\\workSpace\\ocr\\pdf\\2019-12-12送天津4号_1.png";
        return BaiduOCRUtil.ocrImageExcel(BaiduOCRUtil.GetImageBase64(image1));
    }

    @ApiOperation(value = "", notes = "测试条形码识别")
    @PostMapping("/testBarCode")
    @ResponseBody
    public QrcodeResponse testBarCode() {
        // 图片横向
        String image1 = "D:\\workSpace\\ocr\\opencv\\narrow.jpg";
        // 图片倾斜
        // String image1 = "static/images/jingdong/ba7a4eb8689016d4906494ad744ebc3.jpg";
        // 图片竖直
        // String image1 = "static/images/jingdong/303ba519bb1103de3a13075e33dfb9e.jpg";
        // String image1 = "static/images/jingdong/b036c01d28880a39ebd9e76f568b36d.jpg";
        return BaiduOCRUtil.ocrImageQrcode(BaiduOCRUtil.GetImageBase64(image1));
    }

    @ApiOperation(value = "", notes = "测试二维码识别")
    @PostMapping("/testQrocde")
    @ResponseBody
    public QrcodeResponse testQrocde() {
        String image1 = "static/images/qrcode/pda_qrcode.png";
        return BaiduOCRUtil.ocrImageQrcode(BaiduOCRUtil.GetImageBase64(image1));
    }

}
