package com.jlt.baidu.config;

import org.bytedeco.tesseract.global.tesseract;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.jlt.ocr.tesseract.GlobalTessBaseAPI;

import lombok.extern.slf4j.Slf4j;

/**
 * 加载tesseract的中英文字体库
 * 
 * @author 苹果
 * @date 2020/02/18
 */
@Slf4j
@Component
public class TesseractInitConfig implements InitializingBean {

    @Value("${tesseract.path}")
    private String tesseractPath;
    @Autowired
    GlobalTessBaseAPI api;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (tesseractPath == null || "".equals(tesseractPath)) {
            throw new RuntimeException("tesseract path is null");
        }
        if (api.Init(tesseractPath, "chi_sim+eng") != 0) {
            throw new RuntimeException("tesseract init fail");
        }
        api.SetPageSegMode(tesseract.PSM_SINGLE_LINE);
        log.info("====成功加载tesseract模块=====");
    }
}
