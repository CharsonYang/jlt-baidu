package com.jlt.baidu.config;

import org.opencv.core.Core;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 初始化对象后加载opencv,无法兼容springboot的热部署
 * 
 * @author 苹果
 * @date 2019/12/24
 */
@Slf4j
@Component
public class OpencvInitConfig implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        // 启动方式一：写死路径
        // String opencv420 =
        // "D:\\workSpace\\singleWorkSpace\\jlt-baidu\\src\\main\\resources\\opencv\\java\\x64\\opencv_java420.dll";
        // System.load(opencv420);
        // 启动方式二：启动参数添加 路径
        // -Djava.library.path=D:\PData\baidunet\image-process\src\main\resources\lib\;
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        log.info("====加载" + Core.NATIVE_LIBRARY_NAME + "======");
    }

}
