package com.jlt.baidu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableSwaggerBootstrapUI
@SpringBootApplication
@ComponentScan("com.jlt")
public class JltBaiduApplication {

    public static void main(String[] args) {
        SpringApplication.run(JltBaiduApplication.class, args);
    }

}
