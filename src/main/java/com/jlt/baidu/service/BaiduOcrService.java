package com.jlt.baidu.service;

import java.util.List;

import com.jlt.baidu.dto.FileOcrModel;

public interface BaiduOcrService {

    /**
     * 天猫回单识别
     * 
     * @param model
     */
    public FileOcrModel disReceiptForUnderline(FileOcrModel model);

    /**
     * 
     * 天猫回单识别
     * 
     * @param files
     * @return
     */
    public List<FileOcrModel> disReceiptForUnderline(List<FileOcrModel> models);

    /**
     * 
     * 天猫预约单识别
     * 
     * @param files
     * @return
     */
    public List<FileOcrModel> disBookForUnderline(List<FileOcrModel> models);

    /**
     * 天猫预约单识别
     * 
     * @param model
     * @return
     */
    public FileOcrModel disBookForUnderline(FileOcrModel model);
}
