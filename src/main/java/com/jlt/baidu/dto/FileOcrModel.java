package com.jlt.baidu.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 文件cor识别对象
 * 
 * @author 苹果
 * @date 2020/01/06
 */
@Data
public class FileOcrModel implements Serializable {

    Long id;
    /**
     * 文件系统的业务id
     */
    Long fileid;
    /**
     * 文件原始名称
     */
    String fileName;
    /**
     * 生成的uuid
     */
    String uuid;
    /**
     * 文件类型
     */
    String fileType;
    /**
     * 原文件大小
     */
    String fileSize;
    /**
     * 有压缩后，压缩的大小
     */
    String fileCompareSize;
    /**
     * 有压缩后，压缩的路径
     */
    String fileCompareUrl;
    /**
     * 原图文件保存路径
     */
    String fileSaveUrl;
    /**
     * 业务类型 回单，预约单
     */
    String businessType;
    /**
     * 识别结果 0成功 1失败 空未识别
     */
    Integer discernResult;
    /**
     * 失败重试次数 默认1次
     */
    Integer repeatTime;
    /**
     * 请求响应时间差 单位s
     */
    Double requestTime;
    /**
     * 成功的json文本
     */
    String successResult;
    /**
     * 失败的原因
     */
    String failReasons;
}
