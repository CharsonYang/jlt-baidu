package com.jlt.baidu.dto;

import lombok.Data;

@Data
public class CustomExcelResponse {
    long log_id;
    long forms_result_num;
    ExcelItem[] forms_result;

    @Data
    public static class ExcelItem {
        ExcelRow[] body;
    }

    @Data
    public static class ExcelRow {
        int column;
        String words;
        String row;
        String probability;
    }
}
