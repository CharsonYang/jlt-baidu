package com.jlt.baidu.dto;

import lombok.Data;

/**
 * 二维码识别结果
 * 
 * @author 苹果
 * @date 2019/12/21
 */
@Data
public class QrcodeResponse {
    long log_id;
    long codes_result_num;
    Item[] codes_result;

    @Data
    private static class Item {
        String type;
        String text;
    }
}
