package com.jlt.baidu.dto;

import lombok.Data;

/**
 * 文本相似度请求对象.https://ai.baidu.com/docs#/NLP-Basic-API/63eec4cf
 * 
 * @author 苹果
 * @date 2019/10/22
 */
@Data
public class SimilaryRequest {
    String text_1;
    String text_2;
    // 默认为"BOW"，可选"BOW"、"CNN"与"GRNN"
    // BOW（词包）模型 GRNN（循环神经网络）模型 CNN（卷积神经网络）模型
    String model;
}
