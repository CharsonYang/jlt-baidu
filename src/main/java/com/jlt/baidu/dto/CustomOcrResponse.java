package com.jlt.baidu.dto;

import lombok.Data;

/**
 * 自定义的OCR识别
 * 
 * @author 苹果
 * @date 2019/12/26
 */
@Data
public class CustomOcrResponse {
    String error_code;
    String error_msg;
    Item data;

    @Data
    public static class Item {
        String templateSign;
        String templateName;
        String scores;
        boolean isStructured;
        String logId;
        String templateMatchDegree;
        int clockwiseAngle;
        Detail[] ret;
    }

    @Data
    public static class Detail {
        String word_name;
        String word;
        // Probability probability;
        // Location location;
    }

    // @Data
    // private static class Probability {
    // String average;
    // String min;
    // String variance;
    // }
    //
    // @Data
    // private static class Location {
    // String left;
    // String top;
    // String width;
    // String height;
    // }
}
