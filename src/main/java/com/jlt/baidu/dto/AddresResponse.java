package com.jlt.baidu.dto;

import lombok.Data;

/**
 * 地址自动补全返回对象
 * 
 * @author 苹果
 * @date 2019/10/22
 */
@Data
public class AddresResponse {
    long log_id;
    String text;
    String province;
    String province_code;
    String city;
    String city_code;
    String county;
    String county_code;
    String town;
    String town_code;
    String person;
    String detail;
    String phonenum;

    @Override
    public String toString() {
        return "源地址：" + text + "   百度AI匹配地址：" + province + city + county + town + detail;
    }
}
