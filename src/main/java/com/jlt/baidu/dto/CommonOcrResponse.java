package com.jlt.baidu.dto;

import lombok.Data;

public class CommonOcrResponse {
    long log_id;
    /**
     * -1:未定义， - 0:正向， - 1: 逆时针90度， - 2:逆时针180度， - 3:逆时针270度
     */
    int direction;
    int words_result_num;
    Item[] words_result;

    @Data
    private static class Item {
        String words;
        String words_result_idx;
        Location[] location;
        Charsaa[] chars;
    }

    @Data
    private static class Charsaa {
        String ss;
        Location[] location;
    }

    @Data
    private static class Location {
        String left;
        String top;
        String width;
        String height;
    }
}
