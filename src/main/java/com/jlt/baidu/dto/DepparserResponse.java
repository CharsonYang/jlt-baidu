package com.jlt.baidu.dto;

import lombok.Data;

@Data
public class DepparserResponse {
    int id;
    String word;
    String postag;
    int head;
    String deprel;
}
