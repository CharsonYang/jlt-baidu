package com.jlt.baidu.dto;

import java.util.List;

import lombok.Data;

@Data
public class BookBusinessModel {
    /**
     * 清单头
     */
    String headLogId;
    /**
     * 表格内容
     */
    String bodyLogId;
    /**
     * 单据编号
     */
    String orderNo;
    /**
     * 预约日期
     */
    String bookDate;
    /**
     * 预约波次
     */
    String bookWave;
    /**
     * 物流宝单号
     */
    List<String> logisticsNo;
}
