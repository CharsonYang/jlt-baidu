package com.jlt.baidu.dto;

import lombok.Data;

/**
 * 地址自动补全的请求对象
 * 
 * @author 苹果
 * @date 2019/10/22
 */
@Data
public class AddressRequest {
    String text;
}
