package com.jlt.baidu.dto;

import lombok.Data;

@Data
public class ReceiptBusinessModel {
    String logId;
    String templateName;
    /**
     * 业务类型
     */
    String businessType;
    /**
     * 仓库名字
     */
    String warehouseName;
    /**
     * 外部单号
     */
    String logisticsNo;
    /**
     * 预约时间
     */
    String prediectTime;
}
