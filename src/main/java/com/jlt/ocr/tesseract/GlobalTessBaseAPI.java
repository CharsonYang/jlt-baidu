package com.jlt.ocr.tesseract;

import org.bytedeco.tesseract.TessBaseAPI;
import org.springframework.stereotype.Component;

/**
 * 全局定义基础api
 * 
 * @author Ives.Chen
 *
 */
@Component
public class GlobalTessBaseAPI extends TessBaseAPI {

}
