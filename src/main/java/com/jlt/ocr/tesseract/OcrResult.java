package com.jlt.ocr.tesseract;

import lombok.Data;

/**
 * 通用型ocr识别结果
 * 
 * @author Ives.Chen
 *
 */
@Data
public class OcrResult {
    Integer row;
    Integer column;
    // 文本内容
    String words;
    // 相识度
    Double probablity;
}
