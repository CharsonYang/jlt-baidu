package com.jlt.ocr.tesseract;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 文字识别区域
 * 
 * @author Ives.Chen
 *
 */
@AllArgsConstructor
@Data
public class OcrRectangle {
    int left;
    int top;
    int width;
    int height;
}
