package com.jlt.ocr.service;

import java.util.List;

import com.jlt.baidu.dto.FileOcrModel;

public interface TesseractService {
    /**
     * 针对非excel表格内容识别，自动对图片进行表格内容去除
     * 
     * @param models
     * @return
     */
    public List<FileOcrModel> notExcelImageOcr(List<FileOcrModel> models);

    /**
     * 针对excel表格内容识别，自动对图片进行非表格内容去除
     * 
     * @param models
     * @return
     */
    public List<FileOcrModel> excelNotImageOcr(List<FileOcrModel> models);

    /**
     * 通用网络图片，图片全文识别所有内容出来，准确度一般，对英文和数字识别较高
     * 
     * 适合业务场景：只要图片中的某一些数据，不旋转图片
     * 
     * @param models
     * @return
     */
    public List<FileOcrModel> commonImageOcr(List<FileOcrModel> models);

    /**
     * 通用表格图片，将表格数据全部读取，包含列头
     * 
     * @param models
     * @return
     */
    public List<FileOcrModel> excelImageOcr(List<FileOcrModel> models);
}
